﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndTrigger : MonoBehaviour
{
    public Color fadeColor = Color.black;
    public float fadeTime = 2f;
    public float fadeDelay = 1f;
    
    private void OnTriggerEnter(Collider collider)
    {
        CameraFade.StartAlphaFade(fadeColor, false, fadeTime, fadeDelay, () => SceneManager.LoadScene(SceneManager.GetActiveScene().name));
    }
}
