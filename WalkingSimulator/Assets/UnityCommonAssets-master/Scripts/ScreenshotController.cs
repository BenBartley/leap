﻿using UnityEngine;
using System.Collections;

public class ScreenshotController : MonoBehaviour
{
    void Update() {
        if ( Input.GetKeyDown( KeyCode.Alpha1 ) ) {
            ScreenCapture.CaptureScreenshot( "ScreenshotSize1-" + Time.timeSinceLevelLoad + ".png", 1 );
        }
        if ( Input.GetKeyDown( KeyCode.Alpha2 ) ) {
            ScreenCapture.CaptureScreenshot( "ScreenshotSize2-" + Time.timeSinceLevelLoad + ".png", 2 );
        }
        if ( Input.GetKeyDown( KeyCode.Alpha3 ) ) {
            ScreenCapture.CaptureScreenshot( "ScreenshotSize3-" + Time.timeSinceLevelLoad + ".png", 3 );
        }
        if ( Input.GetKeyDown( KeyCode.Alpha4 ) ) {
            ScreenCapture.CaptureScreenshot( "ScreenshotSize4-" + Time.timeSinceLevelLoad + ".png", 4 );
        }
        if ( Input.GetKeyDown( KeyCode.Alpha5 ) ) {
            ScreenCapture.CaptureScreenshot( "ScreenshotSize5-" + Time.timeSinceLevelLoad +  ".png", 5 );
        }
        if ( Input.GetKeyDown( KeyCode.Alpha6 ) ) {
            ScreenCapture.CaptureScreenshot( "ScreenshotSize6-" + Time.timeSinceLevelLoad + ".png", 6 );
        }
        if ( Input.GetKeyDown( KeyCode.Alpha7 ) ) {
            ScreenCapture.CaptureScreenshot( "ScreenshotSize7-" + Time.timeSinceLevelLoad + ".png", 7 );
        }
        if ( Input.GetKeyDown( KeyCode.Alpha8 ) ) {
            ScreenCapture.CaptureScreenshot( "ScreenshotSize8-" + Time.timeSinceLevelLoad + ".png", 8 );
        }
        if ( Input.GetKeyDown( KeyCode.Alpha9 ) ) {
            ScreenCapture.CaptureScreenshot( "ScreenshotSize9-" + Time.timeSinceLevelLoad + ".png", 9 );
        }
        if ( Input.GetKeyDown( KeyCode.Alpha0 ) ) {
            ScreenCapture.CaptureScreenshot( "ScreenshotSize10-" + Time.timeSinceLevelLoad + ".png", 10 );
        }
    }
}
